<?php //session_start();
$_SESSION['baseURL'] = "/theBestLaptops/";
?>
	<header class="header trans_300">

		<!-- Top Navigation -->

		<div class="top_nav">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<!--<div class="top_nav_center">free shipping on all u.s orders over $50</div>-->
					</div>
					<div class="col-md-6 text-right">
						<div class="top_nav_right">
							<ul class="top_nav_menu">
								
								<li class="account">
									<a href="#">
										Mon compte
										<i class="fa fa-angle-down"></i>
									</a>
									<ul class="account_selection">
										<li><a href="<?php echo $_SESSION['baseURL'];?>login"><i class="fa fa-sign-in" aria-hidden="true"></i>Connexion</a></li>
										<li><a href="<?php echo $_SESSION['baseURL'];?>inscription"><i class="fa fa-user-plus" aria-hidden="true"></i>Inscription</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Main Navigation -->

		<div class="main_nav_container">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-right">
						<div class="logo_container">
							<a href="index.php">Get<span>Laptop</span></a>
						</div>
						<nav class="navbar">
							<ul class="navbar_menu">
								<li><a href="index.php">Accueil</a></li>
								<li><a href="categories.php">Categories</a></li>
								<!--<li><a href="#">promotion</a></li>-->
						
								<li><a href="contact.php">contact</a></li>
							</ul>
							<ul class="navbar_user">
								<li><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li>
								<li><a href="login.php"><i class="fa fa-user" aria-hidden="true"></i></a></li>
								
							</ul>
							<div class="hamburger_container">
								<i class="fa fa-bars" aria-hidden="true"></i>
							</div>
						</nav>
					</div>
				</div>
			</div>
		</div>

	</header>

	<div class="fs_menu_overlay"></div>
	<div class="hamburger_menu">
		<div class="hamburger_close"><i class="fa fa-times" aria-hidden="true"></i></div>
		<div class="hamburger_menu_content text-right">
			<ul class="menu_top_nav">
				<li class="menu_item has-children">
					<a href="#">
						usd
						<i class="fa fa-angle-down"></i>
					</a>
					<ul class="menu_selection">
						<li><a href="#">cad</a></li>
						<li><a href="#">aud</a></li>
						<li><a href="#">eur</a></li>
						<li><a href="#">gbp</a></li>
					</ul>
				</li>
		
				<li class="menu_item has-children">
					<a href="#">
						Mon compte
						<i class="fa fa-angle-down"></i>
					</a>
					<ul class="menu_selection">
						<li><a href="login.php"><i class="fa fa-sign-in" aria-hidden="true"></i>Connexion</a></li>
						<li><a href="inscription.php"><i class="fa fa-user-plus" aria-hidden="true"></i>Inscription</a></li>
					</ul>
				</li>
				<li class="menu_item"><a href="#">Accueil</a></li>
				<li class="menu_item"><a href="categories.php">achèter</a></li>
				<li class="menu_item"><a href="contact.php">contact</a></li>
			</ul>
		</div>
	</div>
