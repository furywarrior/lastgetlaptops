<?php
	$titre = "Dashboard";
	
?>
<nav class="navbar">
	 <div class="navbar-header container brand-bleachedcedar">
	   <a href="#" class="menu-toggle"><i class="zmdi zmdi-menu"></i></a>
	   <a href="dashboard.php" class="logo"><img src="img/logo.png" alt="Logo Pacificonis"></a>
	   <a href="dashboard.php" class="icon-logo"></a>
	 </div>
	  <div class="navbar-container clearfix">
		<div class="pull-left">
		  <a href="#" class="page-title text-uppercase"><?php echo $titre; ?></a>
		</div>

		<div class="pull-right">
		  <ul class="nav pull-right right-menu">
			<li class="more-options dropdown">
			  <a class="dropdown-toggle" data-toggle="dropdown">
				<i class="zmdi zmdi-account-circle"></i>
			  </a>
			  <div class="more-opt-container dropdown-menu">
				<a href="#"><i class="zmdi zmdi-account-o"></i>Compte</a>
				<a href="deconnexion.php"><i class="zmdi zmdi-run"></i>Quitter</a>
			  </div>
			</li>
			
			<li>
			  <a class="sidepanel-toggle" href="#">
				<i class="zmdi zmdi-more-vert"></i>
			  </a>
			</li>
		  </ul>
		</div>
	  </div>
</nav>