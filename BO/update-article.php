<?php 
	session_start();
	if(!isset($_SESSION['username'])){
		header('Location: index.php?error=2');
	}
	require_once('inc/model.php');
	$articleList = select_sql('Article', '4<5');
	$categorieList = select_sql('Categorie', '4<5');
	$marqueList = select_sql('Marque', '4<5');
	$idArticle = "";
	if(isset($_GET['id'])){
		$idArticle = $_GET['id'];
	}
	$article = getArticleById($idArticle);
	
	//REFERENCEMENT
	$title = "Mettre &agrave; jour un article";
?>

<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from 91.234.35.26/pacificonis-admin/v1.2.0/back/pro/1_vertical_icon_menu/starter-page.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Apr 2018 19:13:26 GMT -->
<head>

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no" />
  
  
  <!-- Chrome, Firefox OS and Opera -->
  <meta name="theme-color" content="#49CEFF">
  <!-- Windows Phone -->
  <meta name="msapplication-navbutton-color" content="#49CEFF" />
  <!-- iOS Safari -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

  <title><?php echo $title;?></title>
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
  <link rel="stylesheet" href="bower_components/animate.css/animate.min.css">
  <link rel="stylesheet" href="bower_components/metisMenu/dist/metisMenu.min.css">
  <link rel="stylesheet" href="bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
  <link rel="stylesheet" href="bower_components/Waves/dist/waves.min.css">
  <link rel="stylesheet" href="bower_components/toastr/toastr.css">



  <link rel="stylesheet" href="css/style.css">

<link rel="stylesheet" href="css/demo.css">


<!--
  <link rel="icon" href="img/favicon.ico" type="image/x-icon" />
  <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
-->
    <!--[if lt IE 9]>
      <script src="bower_components/html5shiv/dist/html5shiv.min,js"></script>
      <script src="bower_components/respondJs/dest/respond.min.js"></script>
    <![endif]-->
</head>

<body class="icon-menu light-menu fixed-all">
  <!--Preloader-->
<!--<div id="preloader">
      <div class="refresh-preloader"><div class="preloader"><i>.</i><i>.</i><i>.</i></div></div>
</div>-->


  <div class="wrapper">
	<?php include 'header.php'; ?>
	
	<?php include 'menuBar.php'; ?>


    <div class="container-fluid">
      <div class="row">
			<div class="col-md-6">
			  <div class="content-box">
				<div class="head success-bg clearfix">
				  <h5 class="content-title pull-left"><?php echo $title;?>:</h5>
				  <div class="functions-btns pull-right">
					<a class="refresh-btn" href="#"><i class="zmdi zmdi-refresh"></i></a>
					<a class="fullscreen-btn" href="#"><i class="zmdi zmdi-fullscreen"></i></a>
					<a class="close-btn" href="#"><i class="zmdi zmdi-close"></i></a>
				  </div>
				</div>

				<div class="content">
				  
				  <form class="form-horizontal" action="inc/valid-update-delete.php?action=1" method="POST">
					
					 <input type="hidden" name="id" value="<?php echo $idArticle;?>" >
					<div class="form-group">
						<label for="inputText" class="col-sm-2 control-label">Categories : </label>
						<div class="col-sm-10">
						<div class="select">
						  <select class="form-control selectpicker" name="idCategorie">
							<?php foreach($categorieList AS $tab){?>
								<option value="<?php echo $tab['ID'];?>" <?php echo "selected"; ?> ><?php echo $tab['nom'];?></option>
							<?php } ?>
						  </select>
						</div>
						</div>
					</div>
					
					  <div class="form-group ">
					  <label for="inputText2" class="col-sm-2 control-label">Marques : </label>
						<div class="col-sm-10">
						<div class="select">
						  <select class="form-control selectpicker" name="idMarque">
							<?php foreach($marqueList AS $tab){?>
								<option value="<?php echo $tab['ID'];?>"><?php echo $tab['nom'];?></option>
							<?php } ?>
						  </select>
						</div>
						</div>
					  </div>
					
					<div class="form-group ">
					  <label for="inputEmail3" class="col-sm-2 control-label">Nom de l'ordinateur</label>
					  <div class="col-sm-10">
						<input type="text" name="nom" class="form-control" value="<?php echo $article['nom'];?>" id="inputEmail3" placeholder="Nom de l'ordinateur">
					  </div>
					</div>
					
					<div class="form-group form-horizontal">
					  <div class="col-sm-offset-2">
						<button type="submit" class="btn btn-primary m-b-5">Valider</button>
					  </div>
					</div>
				  </form>
				</div>
			  </div>
			</div>
			
			
		
      </div>
    </div>
    <footer class="page-footer">© 2017 Copyright</footer>
  </div>


    
  <button class="btn btn-primary btn-lg icon raised demo-switcher" type="button"><i class="zmdi zmdi-settings"></i></button>

	<?php include "panel.php" ?>
    
  <script src="bower_components/jquery/dist/jquery.min.js"></script>
  <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
  <script src="bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.js"></script>
  <script src="bower_components/Waves/dist/waves.min.js"></script>
  <script src="bower_components/toastr/toastr.js"></script>

    <script src="js/common.js"></script>
  
	<script src="js/demo-switch.js"></script>
  

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../../../../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-96761580-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter43958229 = new Ya.Metrika({
                    id:43958229,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    ut:"noindex"
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "../../../../../../mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/43958229?ut=noindex" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>

<!-- Mirrored from 91.234.35.26/pacificonis-admin/v1.2.0/back/pro/1_vertical_icon_menu/starter-page.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Apr 2018 19:13:26 GMT -->
</html>
