<?php 
	session_start();
	if(!isset($_SESSION['username'])){
		header('Location: index.php?error=2');
	}
	require_once('inc/model.php');
	
	
	$marqueList = select_sql('Marque', '4<5');
	$categorieList = select_sql('Categorie', '4<5');

		//REFERENCEMENT
	$title = "Liste des marques et categories";
?>
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from 91.234.35.26/pacificonis-admin/v1.2.0/back/pro/1_vertical_icon_menu/starter-page.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Apr 2018 19:13:26 GMT -->
<head>

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no" />
  
  
  <!-- Chrome, Firefox OS and Opera -->
  <meta name="theme-color" content="#49CEFF">
  <!-- Windows Phone -->
  <meta name="msapplication-navbutton-color" content="#49CEFF" />
  <!-- iOS Safari -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

  <title><?php echo $title;?></title>
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
  <link rel="stylesheet" href="bower_components/animate.css/animate.min.css">
  <link rel="stylesheet" href="bower_components/metisMenu/dist/metisMenu.min.css">
  <link rel="stylesheet" href="bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
  <link rel="stylesheet" href="bower_components/Waves/dist/waves.min.css">
  <link rel="stylesheet" href="bower_components/toastr/toastr.css">



  <link rel="stylesheet" href="css/style.css">

<link rel="stylesheet" href="css/demo.css">


<!--
  <link rel="icon" href="img/favicon.ico" type="image/x-icon" />
  <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
-->
    <!--[if lt IE 9]>
      <script src="bower_components/html5shiv/dist/html5shiv.min,js"></script>
      <script src="bower_components/respondJs/dest/respond.min.js"></script>
    <![endif]-->
</head>

<body class="icon-menu light-menu fixed-all">
  <!--Preloader-->
<div id="preloader">
      <div class="refresh-preloader"><div class="preloader"><i>.</i><i>.</i><i>.</i></div></div>
</div>


	<div class="wrapper">
	<?php include 'header.php'; ?>
	
	<?php include 'menuBar.php'; ?>

	
    <div class="container-fluid">
      <div class="row">
	  
		<h4><?php echo $title;?></h4>
              <div class="table-responsive alt-table">
                <table class="table table-hover table-bordered" style="width:60%;height:80%">
                    <thead>
                        <tr>
                            <th class="table-check">
                                #
                            </th>
                            <th>Marque</th>
                            <th class="text-center">Options</th>
                        </tr>
                    </thead>
                    <tbody>
					<?php foreach($marqueList AS $marque){ ?>
                        <tr>
                            <td class="table-check">
                                <?php echo $marque['ID']; ?>
                            </td>
                            <td>
                                <?php echo $marque['nom']; ?>
                            </td>
                            
                            <td class="text-center">
                               <div class="btn-group">
                                <a href="update-marque.php?id=<?php echo $marque['ID']; ?>"><button type="button" class="btn btn-success"><i class="zmdi zmdi-edit"></i></button></a>
                                
								<button type="button" class="btn btn-success" onclick="myFunction()"><i class="zmdi zmdi-delete"></i></button>
							<!--<a href="valid-update-delete.php?action=2&type=marque&id=<?php echo $marque['ID']; ?>"><button type="button" class="btn btn-success"><i class="zmdi zmdi-delete"></i></button></a>-->
                              </div>
                            </td>
                        </tr>
					<?php } ?>
                    </tbody>
				
                </table>
				<?php if(isset($_GET['error'])){?>
					<span style="color:red;">Une erreur est survenu!!!</span>
				<?php } ?>
            </div>
        <!-- Content goes here-->
		
	  
		<h4><?php echo $title;?></h4>
              <div class="table-responsive alt-table">
                <table class="table table-hover table-bordered" style="width:60%;height:80%">
                    <thead>
                        <tr>
                            <th class="table-check">
                                #
                            </th>
                            <th>Categorie</th>
                            <th class="text-center">Options</th>
                        </tr>
                    </thead>
                    <tbody>
					<?php foreach($categorieList AS $categorie){ ?>
                        <tr>
                            <td class="table-check">
                                <?php echo $categorie['ID']; ?>
                            </td>
                            <td>
                                <?php echo $categorie['nom']; ?>
                            </td>
                            
                            <td class="text-center">
                               <div class="btn-group">
                                <a href="update-marque.php?id=<?php echo $categorie['ID']; ?>"><button type="button" class="btn btn-success"><i class="zmdi zmdi-edit"></i></button></a>
                                
								<button type="button" class="btn btn-success" onclick="myFunction2()"><i class="zmdi zmdi-delete"></i></button>
							<!--<a href="valid-update-delete.php?action=2&type=categorie&id=<?php echo $categorie['ID']; ?>"><button type="button" class="btn btn-success"><i class="zmdi zmdi-delete"></i></button></a>-->
                              </div>
                            </td>
                        </tr>
					<?php } ?>
                    </tbody>
				
                </table>
				<?php if(isset($_GET['error'])){?>
					<span style="color:red;">Une erreur est survenu!!!</span>
				<?php } ?>
            
        <!-- Content goes here-->
      </div>
      </div>
    </div>
	
    
    <footer class="page-footer">© 2017 Copyright</footer>
  </div>


    
  <button class="btn btn-primary btn-lg icon raised demo-switcher" type="button"><i class="zmdi zmdi-settings"></i></button>

   <?php include "panel.php" ?>
    
  <script src="bower_components/jquery/dist/jquery.min.js"></script>
  <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
  <script src="bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.js"></script>
  <script src="bower_components/Waves/dist/waves.min.js"></script>
  <script src="bower_components/toastr/toastr.js"></script>

    <script src="js/common.js"></script>
  
	<script src="js/demo-switch.js"></script>
  

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../../../../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-96761580-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter43958229 = new Ya.Metrika({
                    id:43958229,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    ut:"noindex"
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "../../../../../../mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<script>	
<!--<a href="valid-update-delete.php?action=2&type=marque&id=<?php echo $marque['ID']; ?>"><button type="button" class="btn btn-success"><i class="zmdi zmdi-delete"></i></button></a>-->
                         
		function myFunction() {
			var txt;
			var r = confirm("Voulez-vous vraiment valider la suppression?");

			if (r == true) {
				
				window.location.href="inc/valid-update-delete.php?action=2&type=marque&id=<?php echo $marque['ID']; ?>";
				console.log("OK!!!");
			} else {
				
				alert("La suppresion a été annulé!!!");
				console.log("NO!!!");
				
			}
			//document.getElementById("demo").innerHTML = txt;
		}	
		function myFunction2() {
			var txt;
			var r = confirm("Voulez-vous vraiment valider la suppression?");

			if (r == true) {
				
				window.location.href="inc/valid-update-delete.php?action=2&type=categorie&id=<?php echo $categorie['ID']; ?>";
				console.log("OK!!!");
			} else {
				
				alert("La suppresion a été annulé!!!");
				console.log("NO!!!");
				
			}
			//document.getElementById("demo").innerHTML = txt;
		}	
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/43958229?ut=noindex" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>

<!-- Mirrored from 91.234.35.26/pacificonis-admin/v1.2.0/back/pro/1_vertical_icon_menu/starter-page.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Apr 2018 19:13:26 GMT -->
</html>
