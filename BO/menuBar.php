<aside class="sidebar sidebar-bleachedcedar" >
  <ul class="nav metismenu">
  
    <li>
      <a href="#"><i class="zmdi zmdi-view-dashboard"></i>Dashboard<span class="zmdi arrow"></span></a>
      <ul class="nav nav-inside collapse">
        <li class="inside-title">Dashboard</li>
		<li><a href="dashboard.php">Liste des ordinateurs</a></li>
		<li><a href="utilisateur.php">Liste des Utilisateurs</a></li>
		<li><a href="marque-categorie.php">Liste des marques et categories</a></li>
      </ul>
    </li>
	<li>
      <a href="#"><i class="zmdi zmdi-border-all"></i>Formulaire<span class="zmdi arrow"></span></a>
      <ul class="nav nav-inside collapse">
			<li class="inside-title">Formulaire</li>
			<li><a href="nouveau.php?type=marque">Nouvelle marque</a></li>
			<li><a href="nouveau.php?type=categorie">Nouvelle categorie</a></li>
			
      </ul>
    </li> 
    <li>
      <a href="nouvelle-page.php"><i class="zmdi zmdi-copy"></i>Nouvelle page</a>
    </li>
  </ul>
</aside>