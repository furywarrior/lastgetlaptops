<?php
	function dbconnect(){
		static $connect = null;
		if ($connect === null) {
			$PARAM_hote='localhost'; // le chemin vers le serveur
			$PARAM_port='3306';
			$PARAM_nom_bd='laptops'; // le nom de votre base de données
			$PARAM_utilisateur='root'; // nom d'utilisateur pour se connecter
			$PARAM_mot_passe='root'; // mot de passe de l'utilisateur pour se connecter
			
			try{
				$connect = new PDO('mysql:host='.$PARAM_hote.';port='.$PARAM_port.';dbname='.$PARAM_nom_bd, $PARAM_utilisateur, $PARAM_mot_passe);
           // echo "Base has been connected!!!";
            }
			catch(Exception $e){
				echo 'Erreur : '.$e->getMessage().'<br />';
			}
		}
		return $connect;
    }
	function select_sql($table, $cond){
		$connexion = dbconnect();
		$requete = "SELECT * from ".$table." where 1<3 and  ".$cond ;	

		//echo $requete."<br/>";
		$sql = $connexion->prepare($requete);
		$sql->execute();
		$rows = $sql->fetchAll();  
        return $rows;
	}
	function delete_sql($table, $id){
		
		$connexion = dbconnect();
		$requete = "DELETE from ".$table." where ID=".$id ;	
		echo "requete = ".$requete."</br>";
		$sql = $connexion->prepare($requete);
		return $sql->execute();
		//echo "delete rows success!!!";  
       // return $rows;
	}
	
	function getNom($table, $id){
		$retour = "";
		foreach($table AS $tab){
			if($tab['ID'] == $id){
				$retour = $tab['nom'];
			}
		}		
		return $retour;
	}
	function getNomDe($table, $id){
		$retour = "";
		$table = select_sql($table, "ID = '".$id."'");
		foreach($table AS $tab){
			if($tab['ID'] == $id){
				$retour = $tab['nom'];
			}
		}		
		return $retour;
	}
	 
	function getCategorieByIdArticle($listCategorie, $listArticles, $idArticle){
		$idCateg = "";
		$retour = "";
		foreach($listArticles AS $tab){
			if($tab['ID'] == $idArticle){
				$idCateg = $tab['idCategorie'];
			}
		}	
		$retour = getNom($listCategorie, $idCateg);

		return $retour;
	}
	
	 
	
	function getImageByIdArticle($idArticle, $imageDetails, $listImg, $listArticle){
		//$idImg = select_sql("Image","idArticle = '".$idArticle."'");
		//$cat = select_sql("Article", "ID= '".$idArticle."'");
		$idImage = "";
		$idCategorie = "";
		foreach($listImg AS $image){
			if($image['idArticle']==$idArticle){
				$idImage = $image['ID'];break;
			}
		}
		foreach($listArticle AS $article){
			if($article['ID']==$idArticle){
				$idCategorie = $article['idCategorie'];break;
			}
		}

		foreach($imageDetails AS $image){
			if($image['idImage']==$idImage){
				$split = explode(".",$image['image']);
				$vita = substr($split[0],0 ,strlen($split[0])-1);
			//	echo "vita = ".$vita;

				$myNom =  getNomDe("Categorie", $idCategorie);
			
				return $myNom."/".$vita."/".$image['image'];
			}
		}
		return -1;
	}
	function generatePath($idArticle, $imageDetails, $listImg, $listArticle){
		//$idImg = select_sql("Image","idArticle = '".$idArticle."'");
		//$cat = select_sql("Article", "ID= '".$idArticle."'");
		$idImage = "";
		$idCategorie = "";
		foreach($listImg AS $image){
			if($image['idArticle']==$idArticle){
				$idImage = $image['ID'];break;
			}
		}
		foreach($listArticle AS $article){
			if($article['ID']==$idArticle){
				$idCategorie = $article['idCategorie'];break;
			}
		}

		foreach($imageDetails AS $image){
			if($image['idImage']==$idImage){
				$split = explode(".",$image['image']);
				$vita = substr($split[0],0 ,strlen($split[0])-1);
			//	echo "vita = ".$vita;

				$myNom =  getNomDe("Categorie", $idCategorie);
			
				return $myNom."/".$vita."/";
			}
		}
		return -1;
	}
	
	function getTableauAssociatif($sexe){
		$note = array();
		$matiere = array();

		$matieres = select_sql('Matiere','1<5');
		$resultats = select_sql('Resultat','1<5');
		$requete = "1<8";
		//echo "SEXE = ".$sexe."   ";
		if($sexe!="*"){
			$requete = "sexe='".$sexe."'";
		}
		else{
			$requete = "2<6";
		}
		//echo "REQUETE = ".$requete."</br>";
		$elevestab = select_sql('Eleve', $requete);

			foreach($matieres AS $mat){
				foreach($elevestab AS $elev){	
					
					if(getNoteParMatiere( $elev['ID'], $mat['ID'], $resultats)!=null){
						$note[getNom($matieres, $mat['ID'])][getNom($elevestab,$elev['ID'])] = getNoteParMatiere( $elev['ID'], $mat['ID'], $resultats);
					}
					else{
						$note[getNom($matieres, $mat['ID'])][getNom($elevestab,$elev['ID'])] = "";			
					}
				}
			}
		
		return $note;

	}
		 
	function insertBlobImage($idImage, $blobImage){
		$requete = "insert into ImageDetail ( idImage, image) values ('%s','%s')";
		$requete=sprintf($requete, $idImage, addslashes($blobImage));
		dbconnect()->exec($requete);
		$list = getLastID('Location');
		//	echo "sizeOF = ".sizeOf($list);

		$retour = 0;
		foreach($list AS $ret){
			//echo "<br/>   id RETOUR === > ".$ret['ID']."<br/>";
			$retour = $ret['ID'];
		}
		return $retour;
	}
	/*function updateUtilisateur($id, $nomUser, $prenomUser, $emailUser, $mdpUser, $statusAdmin, $cond){
		
		$connexion = getConnection();
		$requete = "UPDATE Utilisateur set ID=:id , nomUser=:nomUser, prenomUser=:prenomUser, emailUser=:emailUser, mdpUser=:mdpUser, statusAdmin=:statusAdmin where id=:cond";	
		$sql = $connexion->prepare($requete);
		$sql->execute(array('id'->$id, 'nomUser'->$nomUser, 'prenomUser'->$prenomUser, 'emailUser'->$emailUser, 'mdpUser'->sha1($mdpUser), 'statusAdmin'->$statusAdmin, 'cond'->$cond));
		
	}*/
	function getIdUserByName($name){
		
		$retours = select_sql("utilisateur", "emailUser = '".$name."'");
		//var_dump($retours);
	
		foreach($retours AS $retour){
			
			if($retour['emailUser'] == $name)
				echo "ID == ".$retour['ID'];
				return $retour['ID'];
		}
        return "erreur";
	}
	function diffTwoDates($date1 , $date2){
		$connexion = dbconnect();
		$requete = "SELECT DATEDIFF('".$date1."', '".$date2."') as date";
		$sql = $connexion->prepare($requete);
		$sql->execute();
		$rows = $sql->fetchAll();  
        return $rows[0]['date'];

	}
	function getLastID($table){
		$connexion = dbconnect();
		$requete = "SELECT * from ".$table." order by ID desc limit 1";
		$sql = $connexion->prepare($requete);
		$sql->execute();
		$rows = $sql->fetchAll();  
        return $rows;
	}
	//INSERTION
	function insertUtilisateur($nomUser, $prenomUser, $emailUser, $mdpUser){
		$requete = "insert into Utilisateur ( nomUser, prenomUser, emailUser, mdpUser, statusUser, statusAdmin) values ('%s','%s','%s',".sha1("'%s'").",'%s','%s')";
		echo $requete;
		$requete=sprintf($requete, $nomUser, $prenomUser, $emailUser, $mdpUser, 1, 0 );
		
		dbconnect()->exec($requete);
		$list = getLastID('Location');
	//	echo "sizeOF = ".sizeOf($list);

		$retour = 0;
		foreach($list AS $ret){
			//echo "<br/>   id RETOUR === > ".$ret['ID']."<br/>";
			$retour = $ret['ID'];
		}
		return $retour;
	}
	
	function insertMarqueCategorie($type, $nom){
		$requete = "insert into ".$type." ( ID, nom) values (0,'%s')";
		
		$requete=sprintf($requete, $nom );
		echo $requete;
		return dbconnect()->exec($requete);
		
	}
	
	
	
	//UPDATE
	function updateUtilisateur($id, $nomUser, $prenomUser, $emailUser, $mdpUser, $statusUser, $statusAdmin, $cond){
		$requete = "UPDATE Utilisateur set ID='%s' , nomUser='%s', prenomUser='%s', emailUser='%s', mdpUser='%s', statusUser='%s', statusAdmin='%s' where ID='%s'";
		
		$requete=sprintf($requete, $id, $nomUser, $prenomUser, $emailUser, $mdpUser, $statusUser, $statusAdmin, $cond);
		echo $requete;
		return dbconnect()->exec($requete);
		
		
	}
	function updateArticle($id, $idCategorie, $idMarque, $nom, $cond){
		$requete = "UPDATE Article set ID='%s' , idCategorie='%s', idMarque='%s', nom='%s' where ID='%s'";
		echo "REPERE = ".$id." ".$idCategorie." ".$nom;
		$requete=sprintf($requete, $id, $idCategorie, $idMarque, $nom, $cond);
		echo $requete;
		return dbconnect()->exec($requete);
		
	}
	
	function getUtilisateurById($id){
		$list = select_sql("Utilisateur", "ID = ".$id);
		
		return $list[0];
	}
	function getArticleById($id){
		$list = select_sql("Article", "ID = ".$id);
		
		return $list[0];
	}
    
?>