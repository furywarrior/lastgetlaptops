<?php
	require_once('model.php');
	
	
	if(isset($_GET['action']) && $_GET['action']==1){
		$idArticle="";
		$idCategorie="";
		$idMarque="";
		$nom="";
		/*echo $_POST['id']."</br>";
		echo $_POST['idCategorie']."</br>";
		echo $_POST['idMarque']."</br>";
		echo $_POST['nom']."</br>";
		*/
		if(isset($_POST['id'], $_POST['idCategorie'], $_POST['idMarque'], $_POST['nom'])){
			$idArticle = $_POST['id'];
			$idCategorie = $_POST['idCategorie'];
			$idMarque = $_POST['idMarque'];
			$nom = $_POST['nom'];
		}

		$bool = updateArticle($idArticle, $idCategorie, $idMarque, $nom, $idArticle);
		if($bool){
			header('Location: ../dashboard.php');
		}
		else{
			header('Location: ../dashboard.php?error=1');
		}
	}
	else if(isset($_GET['action']) && $_GET['action']==2){
		$type="";
		if(isset($_GET['type'])){
			$type = $_GET['type'];
		}
		
		if($type=="article"){
			if(isset($_GET['id'])){
				$ret = delete_sql("Article", $_GET['id']);
				echo "Delete = ".$ret;
				if($ret){
					//header('Location: ../dashboard.php');
				}
				else{
					//header('Location: ../dashboard.php?error=1');
				}
			}
		}
		else if($type=="categorie"){
			if(isset($_GET['id'])){
				$ret = delete_sql("Categorie", $_GET['id']);
				if($ret){
					header('Location: ../dashboard.php');
				}
				else{
					header('Location: ../dashboard.php?error=1');
				}
			}
		}
		else if($type=="marque"){
			if(isset($_GET['id'])){
				$ret = delete_sql("Marque", $_GET['id']);
				if($ret){
					header('Location: ../dashboard.php');
				}
				else{
					header('Location: ../dashboard.php?error=1');
				}
			}
		}
		else{
			header('Location: ../dashboard.php?error=1');
		}
		
	}
	
?>