<?php
	require_once('model.php');
	$idUser="";
	if(isset($_GET['id'])){
		$idUser = $_GET['id'];
	}
	$user = getUtilisateurById($idUser);
	//$id, $nomUser, $prenomUser, $emailUser, $mdpUser, $statusUser, $statusAdmin, $cond
	$status = $user['statusUser']==0 ? 1 : 0;
	$bool = updateUtilisateur($user['ID'], $user['nomUser'], $user['prenomUser'], $user['emailUser'], $user['mdpUser'], $status, $statusAdmin, $user['ID']);
	if($bool){
		header('Location: ../utilisateur.php');
	}
	else{
		header('Location: ../utilisateur.php?error=1');
	}

?>