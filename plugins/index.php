<?php
	header("Content-type: text/css; charset=UTF-8");
	ob_start('ob_gzhandler');
	include "font-awesome-4.7.0/css/font-awesome.css";
	include "font-awesome-4.7.0/css/font-awesome.min.css";
	
	include "OwlCarousel2-2.2.1/animate.css";
	include "OwlCarousel2-2.2.1/owl.carousel.css";
	include "OwlCarousel2-2.2.1/owl.theme.default.css";
	
	include "themify-icons/themify-icons.css";
	
	
	ob_end_flush();
?>