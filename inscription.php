<?php
session_start();
$_SESSION['baseURL'] = "/theBestLaptops/";
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Inscription</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Colo Shop Template">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="<?php echo $_SESSION['baseURL'];?>plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!--CONCATENATION CSS debut-->
<link rel="stylesheet" type="text/css" href="<?php echo $_SESSION['baseURL']."plugins/index.php"; ?>" >
<link rel="stylesheet" type="text/css" href="<?php echo $_SESSION['baseURL']."styles/index.php"; ?>" >
<link rel="stylesheet" type="text/css" href="<?php echo $_SESSION['baseURL']."styles/main_styles.css"; ?>" >
<!--CONCATENATION CSS fin-->
</head>

<body>

<div class="super_container">

	<!-- Header -->
		<?php include('header.php');?>
		
	<div class="container contact_container">
		<div class="row">
			<div class="col">

				<!-- Breadcrumbs -->

				<div class="breadcrumbs d-flex flex-row align-items-center">
					<ul>
						<li><a href="<?php echo $_SESSION['baseURL'];?>">Accueil</a></li>
						<li class="active"><a href="<?php echo $_SESSION['baseURL'];?>/inscription,"><i class="fa fa-angle-right" aria-hidden="true"></i>Inscription</a></li>
					</ul>
				</div>

			</div>
		</div>

		<!-- Map Container -->
>

			<div class="col-lg-6 get_in_touch_col">
				<div class="get_in_touch_contents">
					<h1>Inscription</h1>
					<form action="#" method="POST">
						<div>
							<input id="input_name" class="form_input input_name input_ph" type="text" name="name" placeholder="Name" required="required" data-error="Name is required.">
							<input id="input_email" class="form_input input_email input_ph" type="email" name="email" placeholder="Email" required="required" data-error="Valid email is required.">
							<input id="input_website" class="form_input input_website input_ph" type="url" name="name" placeholder="Website" required="required" data-error="Name is required.">
                        </div>
						<div>
							<button id="review_submit" type="submit" class="red_button message_submit_btn trans_300" value="Submit">Valider</button>
						</div>
					</form>
				</div>
			</div>

		</div>
	</div>

	<!-- Footer -->
	<?php include('footer.php');?>

</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/Isotope/isotope.pkgd.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCIwF204lFZg1y4kPSIhKaHEXMLYxxuMhA"></script>
<script src="plugins/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
<script src="js/contact_custom.js"></script>
</body>

</html>
