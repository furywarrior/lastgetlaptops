create database laptops;
use laptops;
/*Categorie: netbook , gamer , apple*/
create table Categorie(
	ID INTEGER NOT NULL AUTO_INCREMENT primary key,
	nom varchar(50))Engine=InnoDB;

create table Marque(
	ID INTEGER NOT NULL AUTO_INCREMENT primary key,
	nom varchar(50))Engine=InnoDB;
	
	
create table Article(
	ID INTEGER NOT NULL AUTO_INCREMENT primary key,
	idCategorie int,
	idMarque int,
	nom varchar(50),
	/*url varchar(50),*/
	foreign key (idCategorie) references Categorie(ID),
	foreign key (idMarque) references Marque(ID))Engine=InnoDB;

create table Image(
	ID INTEGER NOT NULL AUTO_INCREMENT primary key,
	idArticle int,
	foreign key (idArticle) references Article(ID)
	)Engine=InnoDB;
	
create table ImageDetail(
	ID INTEGER NOT NULL AUTO_INCREMENT primary key,
	idImage int,
	image BLOB,
	foreign key (idImage) references Image(ID))Engine=InnoDB;
	
	
create table ArticleDetail(
	ID INTEGER NOT NULL AUTO_INCREMENT primary key,
	idArticle int,
	CPU varchar(100),
	tailleEcran varchar(100),
	RAM varchar(100),
	disque varchar(100),
	processeurGraphic  varchar(100),
	prix float,
	idImage int,
	foreign key (idArticle) references Article(ID),
	foreign key (idImage) references Image(ID))Engine=InnoDB;

create table Utilisateur(
	ID INTEGER NOT NULL AUTO_INCREMENT primary key,
	nomUser varchar(50),
	prenomUser varchar(50),
	emailUser varchar(50),
	mdpUser varchar(50),
	statusUser int,
	statusAdmin int
	)Engine=InnoDB;
INSERT INTO Utilisateur values(0,'Lock to hang', 'Angelo','parkedenne@gmail.com',sha1('qwerty'), 1, 0);
INSERT INTO Utilisateur values(0,'Lock', 'Julio','julio@gmail.com',sha1('azerty'), 1, 0);
INSERT INTO Utilisateur values(0,'Admin', 'admin','admin@gmail.com',sha1('admin'), 1, 1);


	
	
insert into Categorie values(0, 'netbook');
insert into Categorie values(0, 'gamer');
insert into Categorie values(0, 'apple');


insert into Marque values(0, 'apple');
insert into Marque values(0, 'asus');
insert into Marque values(0, 'dell');
insert into Marque values(0, 'hp');
insert into Marque values(0, 'lenovo');
insert into Marque values(0, 'microsoft');

/*Asus ROG Zephyrus GX501*/
insert into Article values(0, 2, 2, 'Asus ROG Zephyrus GX501');
insert into Image values(0, 1);
insert into ImageDetail values(0, 1, 'rogZephyrus1.jpg');
insert into ImageDetail values(0, 1, 'rogZephyrus2.jpg');
insert into ImageDetail values(0, 1, 'rogZephyrus3.jpg');
insert into ImageDetail values(0, 1, 'rogZephyrus4.jpg');
/*insert into ImageDetail values(0, 1, 'rogZephyrus5.jpg');*/
insert into ArticleDetail values(0, 1, 'Intel Core i7-7700HQ (quad core 3.8GHz)',
 '15.6-inch', '16GB DDR4',  '512GB SSD',
 'Nvidia GeForce GTX 1080 + Intel HD Graphics 630', 2007.90, 1);
 
 /*Apple Macbook Pro*/
insert into Article values(0, 3, 1, 'Apple Macbook Pro with Touch Bar');
insert into Image values(0, 2);
insert into ImageDetail values(0, 2, 'Macbook1.jpg');
insert into ImageDetail values(0, 2, 'Macbook2.jpg');
insert into ImageDetail values(0, 2, 'Macbook3.jpg');
insert into ImageDetail values(0, 2, 'Macbook4.jpg');

insert into ArticleDetail values(0, 2, 'Dual-core Intel Core i5 – i7',
 '13.3-inch, (2,560 x 1,600) IPS', '8GB – 16GB', '256GB – 1TB PCIe 3.0 SSD',
 'Intel Iris Plus Graphics 640 – 650', 1453.99, 2);

 /*Dell Inspiron 15*/
insert into Article values(0, 2, 3, 'Dell Inspiron 15 7000 Gaming');
insert into Image values(0, 3);
insert into ImageDetail values(0, 3, 'Dellinspiron1.jpg');
insert into ImageDetail values(0, 3, 'Dellinspiron2.jpg');

insert into ArticleDetail values(0, 3, 'Intel Core i5-7300HQ (quad core 3.5GHz)',
 '15.6-inch', '8GB DDR4', '256GB SSD',
 'Nvidia GeForce GTX 1050 Ti + Intel HD Graphics 630', 701, 3);

/*Dell Inspiron XPS 15*/
insert into Article values(0, 2, 3, 'Dell XPS 15');
insert into Image values(0, 4);
insert into ImageDetail values(0, 4, 'Dellxps1.jpg');
insert into ImageDetail values(0, 4, 'Dellxps2.jpg');
insert into ImageDetail values(0, 4, 'Dellxps3.jpg');
insert into ImageDetail values(0, 4, 'Dellxps4.jpg');
/*insert into ImageDetail values(0, 4, 'Dellxps5.jpg');*/

insert into ArticleDetail values(0, 4, 'Intel Core i5-7300HQ - i7-7700HQ',
 'Up to 15.6-inch Ultra HD (3840 x 2160) InfinityEdge touchscreen',
 '8GB DDR4', '1TB HDD - 512GB SSD',
 ' NVIDIA® GeForce GTX 1050 with 4GB GDDR5 ', 1249.95, 4);
 
/*MacBook 12-inch*/
insert into Article values(0, 3, 1, 'Apple MacBook 12-inch (2017)');
insert into Image values(0, 5);
insert into ImageDetail values(0, 5, '12inch1.jpg');
insert into ImageDetail values(0, 5, '12inch2.jpg');
insert into ImageDetail values(0, 5, '12inch3.jpg');
insert into ImageDetail values(0, 5, '12inch4.jpg');

insert into ArticleDetail values(0, 5, 'Intel Core M3 1.2GHz - Intel Core i7 1.4GHz',
 '12-inch, (2,304 x 1,440) IPS 16:10',
 '8GB – 16GB', '512GB SSD',
 'Intel HD Graphics 615', 1259, 5);
 
/*Surface book 2*/
insert into Article values(0, 1, 6, 'Microsoft Surface Book 2 (13.5-inch)');
insert into Image values(0, 6);
insert into ImageDetail values(0, 6, 'surfaceBook1.jpg');
insert into ImageDetail values(0, 6, 'surfaceBook2.jpg');
insert into ImageDetail values(0, 6, 'surfaceBook3.jpg');
insert into ImageDetail values(0, 6, 'surfaceBook4.jpg');
/*insert into ImageDetail values(0, 6, 'surfaceBook5.jpg');*/

insert into ArticleDetail values(0, 6, 'Intel Core i5-7300U - Intel Core i7-8650U 1.9GHz',
 '3,000 x 2,000 (267 ppi) PixelSense display, 3:2 aspect ratio',
 '8GB – 16GB', '256GB – 1TB SSD',
 'Nvidia GeForce GTX 1050 (2GB GDDR5 VRAM)', 1369.19, 6);

/*Asus Chromebook Flip*/
insert into Article values(0, 1, 2, 'Asus Chromebook Flip');
insert into Image values(0, 7);
insert into ImageDetail values(0, 7, 'chromeBook1.jpg');
insert into ImageDetail values(0, 7, 'chromeBook2.jpg');
insert into ImageDetail values(0, 7, 'chromeBook3.jpg');
insert into ImageDetail values(0, 7, 'chromeBook4.jpg');
/*insert into ImageDetail values(0, 7, 'chromeBook5.jpg');*/

insert into ArticleDetail values(0, 7, 'Intel Pentium 4405Y – Intel Core m3-6Y30',
 '12.5-inch, FHD (1,920 x 1,080) LED backlit anti-glare',
 ' 4GB', '32GB – 64GB eMMC',
 'Intel HD Graphics 515', 539.84, 7);

/*Lenovo Yoga 920*/
insert into Article values(0, 1, 5, 'Lenovo Yoga 920');
insert into Image values(0, 8);
insert into ImageDetail values(0, 8, 'yoga1.jpg');
insert into ImageDetail values(0, 8, 'yoga2.jpg');
insert into ImageDetail values(0, 8, 'yoga3.jpg');
insert into ImageDetail values(0, 8, 'yoga4.jpg');
/*insert into ImageDetail values(0, 8, 'yoga5.jpg');*/

insert into ArticleDetail values(0, 8, 'Intel Core i7-855OU',
 '13.9-inch 1920 x 1080 - 13.9-inch 3840 x 2160',
 '8GB – 16GB', ' 256GB – 1TB SSD',
 'Intel UHD Graphics 620', 2399, 8);



