<?php
	function dbconnect(){
		static $connect = null;
		if ($connect === null) {
			$PARAM_hote='localhost'; // le chemin vers le serveur
			$PARAM_port='3306';
			$PARAM_nom_bd='laptops'; // le nom de votre base de données
			$PARAM_utilisateur='root'; // nom d'utilisateur pour se connecter
			$PARAM_mot_passe='root'; // mot de passe de l'utilisateur pour se connecter
			
			try{
				$connect = new PDO('mysql:host='.$PARAM_hote.';port='.$PARAM_port.';dbname='.$PARAM_nom_bd, $PARAM_utilisateur, $PARAM_mot_passe);
           // echo "Base has been connected!!!";
            }
			catch(Exception $e){
				echo 'Erreur : '.$e->getMessage().'<br />';
			}
		}
		return $connect;
    }
	function select_sql($table, $cond){
		$connexion = dbconnect();
		
		$requete = "SELECT * from ".$table." where 1<3 and  ".$cond ;	
		if($table=="Article"){
			//echo "requete = ".$requete;
		}
		//echo $requete."<br/>";
		$sql = $connexion->prepare($requete);
		$sql->execute();
		$rows = $sql->fetchAll();  
        return $rows;
	}
	function getNom($table, $id){
		$retour = "";
		foreach($table AS $tab){
			if($tab['ID'] == $id){
				$retour = $tab['nom'];
			}
		}		
		return $retour;
	}
	function getNomDe($table, $id){
		$retour = "";
		$table = select_sql($table, "ID = '".$id."'");
		foreach($table AS $tab){
			if($tab['ID'] == $id){
				$retour = $tab['nom'];
			}
		}		
		return $retour;
	}
	 
	function getCategorieByIdArticle($listCategorie, $listArticles, $idArticle){
		$idCateg = "";
		$retour = "";
		foreach($listArticles AS $tab){
			if($tab['ID'] == $idArticle){
				$idCateg = $tab['idCategorie'];
			}
		}	
		$retour = getNom($listCategorie, $idCateg);

		return $retour;
	}
	function getIdCategorieByName($listCategorie, $name){
		$idCateg = "";
		$retour = "";
		foreach($listCategorie AS $tab){
			if($tab['nom'] == $name){
				$idCateg = $tab['ID'];
			}
		}	
		$retour = $idCateg;

		return $retour;
	}
	
	
	 
	
	function getImageByIdArticle($idArticle, $imageDetails, $listImg, $listArticle){
		//$idImg = select_sql("Image","idArticle = '".$idArticle."'");
		//$cat = select_sql("Article", "ID= '".$idArticle."'");
		$idImage = "";
		$idCategorie = "";
		foreach($listImg AS $image){
			if($image['idArticle']==$idArticle){
				$idImage = $image['ID'];break;
			}
		}
		foreach($listArticle AS $article){
			if($article['ID']==$idArticle){
				$idCategorie = $article['idCategorie'];break;
			}
		}

		foreach($imageDetails AS $image){
			if($image['idImage']==$idImage){
				$split = explode(".",$image['image']);
				$vita = substr($split[0],0 ,strlen($split[0])-1);
			//	echo "vita = ".$vita;

				$myNom =  getNomDe("Categorie", $idCategorie);
				//echo "My idCAtegorie = ".$myNom;
			
				return $myNom."/".$vita."/".$image['image'];
			}
		}
		return -1;
	}
	function generatePath($idArticle, $imageDetails, $listImg, $listArticle){
		//$idImg = select_sql("Image","idArticle = '".$idArticle."'");
		//$cat = select_sql("Article", "ID= '".$idArticle."'");
		$idImage = "";
		$idCategorie = "";
		foreach($listImg AS $image){
			if($image['idArticle']==$idArticle){
				$idImage = $image['ID'];break;
			}
		}
		foreach($listArticle AS $article){
			if($article['ID']==$idArticle){
				$idCategorie = $article['idCategorie'];break;
			}
		}

		foreach($imageDetails AS $image){
			if($image['idImage']==$idImage){
				$split = explode(".",$image['image']);
				$vita = substr($split[0],0 ,strlen($split[0])-1);
			//	echo "vita = ".$vita;

				$myNom =  getNomDe("Categorie", $idCategorie);
			
				return $myNom."/".$vita."/";
			}
		}
		return -1;
	}
	function generateURL($nom, $delimiter){
		$split = explode($delimiter,$nom);
		$taille = sizeOf($split);
		$ret = "";
		for($i=0;$i<$taille;$i++){
			if($i<$taille-1){
				$ret.= $split[$i]."-";
			}
			else{
				$ret.= $split[$i];
			}
			
		}
		return $ret;
	}
	
	function getTableauAssociatif($sexe){
		$note = array();
		$matiere = array();

		$matieres = select_sql('Matiere','1<5');
		$resultats = select_sql('Resultat','1<5');
		$requete = "1<8";
		//echo "SEXE = ".$sexe."   ";
		if($sexe!="*"){
			$requete = "sexe='".$sexe."'";
		}
		else{
			$requete = "2<6";
		}
		//echo "REQUETE = ".$requete."</br>";
		$elevestab = select_sql('Eleve', $requete);

			foreach($matieres AS $mat){
				foreach($elevestab AS $elev){	
					
					if(getNoteParMatiere( $elev['ID'], $mat['ID'], $resultats)!=null){
						$note[getNom($matieres, $mat['ID'])][getNom($elevestab,$elev['ID'])] = getNoteParMatiere( $elev['ID'], $mat['ID'], $resultats);
					}
					else{
						$note[getNom($matieres, $mat['ID'])][getNom($elevestab,$elev['ID'])] = "";			
					}
				}
			}
		
		return $note;

	}
		 
	function insertBlobImage($idImage, $blobImage){
		$requete = "insert into ImageDetail ( idImage, image) values ('%s','%s')";
		$requete=sprintf($requete, $idImage, addslashes($blobImage));
		dbconnect()->exec($requete);
		$list = getLastID('Location');
		//	echo "sizeOF = ".sizeOf($list);

		$retour = 0;
		foreach($list AS $ret){
			//echo "<br/>   id RETOUR === > ".$ret['ID']."<br/>";
			$retour = $ret['ID'];
		}
		return $retour;
	}
	
    
?>