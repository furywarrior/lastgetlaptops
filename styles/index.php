<?php
	header("Content-type: text/css; charset=UTF-8");
	ob_start('ob_gzhandler');
	include "categories_responsive.css";
	include "categories_styles.css";
	include "contact_responsive.css";
	include "contact_styles.css";
	include "main_styles.css";
	include "responsive.css";
	include "single_responsive.css";
	include "single_styles.css";
	include "bootstrap4/bootstrap.min.css";
	ob_end_flush();
?>