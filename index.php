<?php
session_start();
$_SESSION['baseURL'] = "/theBestLaptops/";

	require_once('inc/model.php');
	$imagesDetails = select_sql('ImageDetail', '4<5');
	$categorieList = select_sql('Categorie', '4<5');
	$marqueList = select_sql('Marque', '4<5');
	$articleList = select_sql('Article', '4<5');
	$imageList = select_sql('Image', '4<5');

	echo getImageByIdArticle('2', $imagesDetails, $imageList, $articleList);
	
	
?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>The Best Laptops</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Colo Shop Template">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!--CONCATENATION CSS debut-->
<link rel="stylesheet" type="text/css" href="<?php echo "plugins/index.php"; ?>" >
<link rel="stylesheet" type="text/css" href="<?php echo "styles/index.php"; ?>" >
<link rel="stylesheet" type="text/css" href="<?php echo $_SESSION['baseURL']."styles/main_styles.css"; ?>" >
<!--CONCATENATION CSS fin-->
</head>

<body>

<div class="super_container">

	<!-- Header -->
		<?php include('header.php');?>
	<!-- Slider -->

	<div class="main_slider" style="background-image:url(images/slider_1.jpg)">
		<div class="container fill_height">
			<div class="row align-items-center fill_height">
				<div class="col">
					<div class="main_slider_content">
						<h6>Ordinateurs portable collection 2017</h6>
						<h1>Catalogue sur les meilleurs pc portables</h1>
						
						<div class="red_button shop_now_button"><a href="categories.php">Voir maintenant</a></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Banner -->
<?php 
	$url = array();
	$url[0] = "categories/serie-notebook-1";
	$url[1] = "categories/serie-gamer-2";
	$url[2] = "categories/serie-apple-3";
?>
	<div class="banner">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="banner_item align-items-center" style="background-image:url(images/banner_1.jpg)">
						<div class="banner_category">
							<a href="<?php echo $_SESSION['baseURL'];?><?php echo $url[0]; ?>.html">serie notebooks</a>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="banner_item align-items-center" style="background-image:url(images/banner_2.jpg)">
						<div class="banner_category">
							<a href="<?php echo $_SESSION['baseURL'];?><?php echo $url[1]; ?>.html">serie gamers</a>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="banner_item align-items-center" style="background-image:url(images/banner_3.jpg)">
						<div class="banner_category">
							<a href="<?php echo $_SESSION['baseURL'];?><?php echo $url[2]; ?>.html">serie apples</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Nouveautés -->

	<div class="new_arrivals">
		<div class="container">
			<div class="row">
				<div class="col text-center">
					<div class="section_title new_arrivals_title">
						<h2>Nouveaut&eacute;s</h2>
					</div>
				</div>
			</div>
			<div class="row align-items-center">
				<div class="col text-center">
					<div class="new_arrivals_sorting">
						<ul class="arrivals_grid_sorting clearfix button-group filters-button-group">
							<li class="grid_sorting_button button d-flex flex-column justify-content-center align-items-center active is-checked" data-filter="*">tous</li>
							<?php
								foreach($categorieList AS $categ){
							?>
								<li class="grid_sorting_button button d-flex flex-column justify-content-center align-items-center" data-filter=".<?php echo $categ['nom']?>"><?php echo $categ['nom']?>s</li>
							<?php } ?>
							
						</ul>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="product-grid" data-isotope='{ "itemSelector": ".product-item", "layoutMode": "fitRows" }'>

						<!-- Product 1 -->
						<?php
						foreach($categorieList AS $categ){
							
							$articleByCateg = select_sql('Article', "idCategorie = '".$categ['ID']."'");
							foreach($articleByCateg AS $list){	
							
								$temp = generateURL($list['nom']," ");
								$saveURL = "fiche-produit/article-".$temp;
						?>
							<div class="product-item <?php echo  $categ['nom'];?>">
								<div class="product discount product_filter" heigth="200px">
									<div class="product_image">
										<img src="images/<?php echo getImageByIdArticle($list['ID'], $imagesDetails, $imageList, $articleList);?>" alt="<?php echo $list['nom'];?>">
									</div>
									<div class="favorite favorite_left"></div>
									
									<div class="product_info">
										<h6 class="product_name"><a href="<?php echo $saveURL; ?>-<?php echo $list['ID']; ?>.html"><?php echo $list['nom'];?></a></h6>
										
									</div>
								</div>
		
								<div class="red_button add_to_cart_button"><a href="<?php echo $saveURL; ?>-<?php echo $list['ID']; ?>.html">Voir descriptif</a></div>
							</div>
						<?php }
						} ?>
						<!-- Product 2 -->
						
					</div>
				</div>
			</div>
		</div>
	</div>


	<!-- Footer -->

	<?php include('footer.php');?>

</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/Isotope/isotope.pkgd.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="js/custom.js"></script>
</body>

</html>
