<?php
session_start();
$_SESSION['baseURL'] = "/theBestLaptops/";
	require_once('inc/model.php');
	$imagesDetails = null;
	$categorieList = select_sql('Categorie', '4<5');
	$marqueList = select_sql('Marque', '4<5');
	$articleList = select_sql('Article', '4<5');
	$imageList = select_sql('Image', '4<5');
	$articleDetailList = null;
	$categorie = "";
	$idArticle = "";
	$generateArticle="fiche-produit/article-";
	if(isset($_GET['idArticle'])){
		$idArticle = $_GET['idArticle'];
		$categorie = getCategorieByIdArticle($categorieList ,$articleList , $_GET['idArticle']);
		$articleDetailList = select_sql('ArticleDetail', "idArticle = '".$idArticle."'");
		$imagesDetails = select_sql('ImageDetail', "idImage = '".$idArticle."'");
		if(isset($_GET['url'])){
		$generate .=$_GET['url']."-".$idArticle.".html";
		}
	}
	

	$CPU = $articleDetailList[0]['CPU'];
	$tailleEcran = $articleDetailList[0]['tailleEcran'];
	$RAM = $articleDetailList[0]['RAM'];
	$disque = $articleDetailList[0]['disque'];
	$processeurGraphic = $articleDetailList[0]['processeurGraphic'];
	$prix = $articleDetailList[0]['prix'];
	//echo getImageByIdArticle('2', $imagesDetails, $imageList, $articleList);
	
	//URLCategorie
	$idCategorie = getIdCategorieByName($categorieList, $categorie);
	$categNom = getCategorieByIdArticle($categorieList, $articleList, $idArticle);
	$generateCategorie="categories/serie-".$categNom."-";
	if(isset($_GET['url'])){
		$generate .=$_GET['url']."-".$idArticle.".html";
	}
	
	

?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Fiche produits</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Colo Shop Template">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="<?php echo $_SESSION['baseURL'];?>plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!--CONCATENATION CSS debut-->
<link rel="stylesheet" type="text/css" href="<?php echo $_SESSION['baseURL']."plugins/index.php"; ?>" >
<link rel="stylesheet" type="text/css" href="<?php echo $_SESSION['baseURL']."styles/index.php"; ?>" >
<link rel="stylesheet" type="text/css" href="<?php echo $_SESSION['baseURL']."styles/main_styles.css"; ?>" >
<!--CONCATENATION CSS fin-->
</head>

<body>

<div class="super_container">

	<!-- Header -->
	<?php include('header.php');?>

	<div class="container single_product_container">
		<div class="row">
			<div class="col">

				<!-- Breadcrumbs -->

				<div class="breadcrumbs d-flex flex-row align-items-center">
					<ul>
						<li><a href="<?php echo $_SESSION['baseURL'];?>index">Accueil</a></li>
						<li><a href="<?php echo $_SESSION['baseURL'];?><?php echo $generateCategorie."".$idCategorie.".html";?>"><i class="fa fa-angle-right" aria-hidden="true"></i><?php echo $categorie; ?></a></li>
						<li class="active"><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i><?php echo getNom($articleList, $idArticle); ?></a></li>
					</ul>
				</div>

			</div>
		</div>

		<div class="row">
			<div class="col-lg-7">
				<div class="single_product_pics">
					<div class="row">
						<div class="col-lg-3 thumbnails_col order-lg-1 order-3">
							<div class="single_product_thumbnails">
								<ul>
								<?php 
									$path = generatePath($idArticle, $imagesDetails, $imageList, $articleList);
									for($i = 0; $i < sizeOf($imagesDetails) ;$i++){ ?>
										<li><img src="<?php echo $_SESSION['baseURL'];?>images/<?php echo $path."".$imagesDetails[$i]['image'] ;?>" alt="<?php getNom($articleList, $idArticle);?>" data-image="<?php echo $_SESSION['baseURL'];?>images/<?php echo $path."".$imagesDetails[$i]['image'] ;?>"></li>
									<?php } ?>
								</ul>
							</div>
						</div>
						<div class="col-lg-9 image_col order-lg-2 order-1">
							<div class="single_product_image">
								<div class="single_product_image_background" style="background-image:url(<?php echo $_SESSION['baseURL'];?>images/<?php echo $path."".$imagesDetails[0]['image'] ;?>)"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-5">
				<div class="product_details">
					<div class="product_details_title">
					<h2><?php echo getNom($articleList, $idArticle); ?></h2>
						<p>
							<strong>CPU:  </strong> <?php echo $CPU; ?>  
						</p>
						<p>	
							<strong>Graphique:  </strong> <?php echo $processeurGraphic; ?>  
						</p>
						<p>
							<strong>RAM:  </strong> <?php echo $RAM; ?> 
						</p>
						<p>
							<strong>Taille écran:  </strong> <?php echo $tailleEcran; ?>  
						</p>
						<p>
							<strong>Disque dure:  </strong> <?php echo $disque; ?>
						</p>
						<p>
							<strong>Prix:  </strong> <?php echo $prix; ?> dollar(s)
						</p>
					</div>
				
					<div class="product_color">
						<span>Choix du couleur:</span>
						<ul>
							<li style="background: #e54e5d"></li>
							<li style="background: #252525"></li>
							<li style="background: #60b3f3"></li>
						</ul>
					</div>
					<div class="quantity d-flex flex-column flex-sm-row align-items-sm-center">
						<span>Action:</span>
						
						<div class="red_button add_to_cart_button"><a href="#">Favoris</a></div>
						<div class="product_favorite d-flex flex-column align-items-center justify-content-center"></div>
					</div>
				</div>
			</div>
		</div>

	</div>

	<!-- Tabs -->
	
	<div class="tabs_section_container">

		<div class="container">
			<div class="row">
				<div class="col">
					<div class="tabs_container">
						<ul class="tabs d-flex flex-sm-row flex-column align-items-left align-items-md-center justify-content-center">
							<li class="tab active" data-active-tab="tab_1"><span>Description</span></li>
							<li class="tab" data-active-tab="tab_2"><span>Article similaire</span></li>
							
						</ul>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">

					<!-- Tab Description -->

					<div id="tab_1" class="tab_container active">
						<div class="row">
							<div class="col-lg-5 desc_col">
								<div class="tab_title">
									<h4>Description</h4>
								</div>
								<div class="tab_text_block">
									<h2><?php echo getNom($articleList, $idArticle); ?></h2>
										<p>
											<strong>CPU:  </strong> <?php echo $CPU; ?>  
										</p>
										<p>	
											<strong>Graphique:  </strong> <?php echo $processeurGraphic; ?>  
										</p>
										<p>
											<strong>RAM:  </strong> <?php echo $RAM; ?> 
										</p>
										<p>
											<strong>Taille écran:  </strong> <?php echo $tailleEcran; ?>  
										</p>
										<p>
											<strong>Disque dure:  </strong> <?php echo $disque; ?>
										</p>
										<h4>
											<strong>Prix:  </strong> <?php echo $prix; ?> dollar(s)
										</h4>
								</div>
							</div>
							
						</div>
					</div>

					<!-- Tab Additional Info -->

					<div id="tab_2" class="tab_container">
						<div class="row">
							<div class="col additional_info_col">
								<div class="tab_title additional_info_title">
									<h4>Article similaire</h4>
								</div>
								<p>Aucune article similaire à cette article</p>
								
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>

	</div>


	<!-- Footer -->
		<?php include('footer.php');?>

</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/Isotope/isotope.pkgd.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
<script src="js/single_custom.js"></script>
</body>

</html>
